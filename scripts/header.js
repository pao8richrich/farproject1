
const TheHeader = styled.header`
    background:linear-gradient(0deg, rgba(0, 0, 0, 0.7), rgba(59, 59, 59, 0.5)),url("./images/Fondo1.jpg");
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    color: white;
    padding: 10px 50px;
    min-height: 15vh;
    align-items: center;
    justify-content: center;
`;
const Title = styled.h1`
    color: white;
`;
const SubTitle = styled.p`
    font-size: 20px;
    color: white;
    font-weight: 300;
    text-transform: initial;
`;
const Strong = styled.b`
    font-weight: bold;
`;
function Header (props){
    return(
        <TheHeader>
        <Title>Hoteles {props.name}</Title>
        <SubTitle>Desde el <Strong>{formatDate(props.initialDate) }  </Strong> hasta el <Strong> {formatDate(props.finalDate)} </Strong></SubTitle>
        </TheHeader>
    );
}