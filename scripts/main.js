


const TheMain = styled.main`
  background-color: #ffffff;
  padding: 20px 20px;
  display: flex;
  flex-wrap: nowrap;
  flex-wrap: wrap;
  align-content: space-between;
  align-items: center;
  justify-content: center;
  min-height:80px;
`;
const Message = styled.div`
  border: solid rgb(230, 230, 230) 1px;
  border-radius: 2px;
  background-color: white;
  -webkit-box-shadow: -1px 2px 7px 0px rgba(0,0,0,0.25);
  -moz-box-shadow: -1px 2px 7px 0px rgba(0,0,0,0.25);
  box-shadow: -1px 2px 7px 0px rgba(0,0,0,0.25);
  width: 30%;
  margin: 16px;
  border-radius: 15px;
  overflow: hidden;
  height: auto;
  text-align: center;
  padding: 30px;
  align-items: center;
  justify-content: center;
  color:grey;
`;
const StrongMessage = styled.div`
  font-weight: 800;
  font-size: 25px;
  color: #646464;
  padding-bottom: 20px;
`;
function Main (props){

  const finalHotelFilter = hotelsData.filter(
    (hotel) => {
      let selectDate = props.selectInitial.valueOf() >= hotel.availabilityFrom && props.selectFinal.valueOf() <= hotel.availabilityTo;
      let country = props.selectCountry === "Todos los países" ? true : hotel.country == props.selectCountry ;
      let price = props.selectPrice === "Cualquier Precio" ? true : hotel.price == props.selectPrice.length;
      let selectSize = hotel.rooms
      
      switch(props.selectSize){
          case 'Hotel pequeño':
            selectSize = hotel.rooms <=10
            break
          case 'Hotel mediano':
            selectSize = hotel.rooms >= 11 && hotel.rooms  <= 20
            break
          case 'Hotel grande':
            selectSize = hotel.rooms >= 21
            break
          default:
            selectSize = hotel.rooms
            break
      }
      return selectDate  && country && price && selectSize ;
    } 
  )
  return(
      <TheMain>
       {finalHotelFilter.length >0 ? finalHotelFilter.map(
          hotel  => <Card key={hotel.slug} hotel={hotel}  />
        ): <Message><StrongMessage>No disponible </StrongMessage>  No se encuentran hoteles disponibles con esa selección </Message>}
      
      </TheMain>
  );
}