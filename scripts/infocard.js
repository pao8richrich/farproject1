const TitleCard = styled.div`
    font-size: 24px;
    font-weight: 600;
    color: rgb(93, 93, 93);
    text-transform: capitalize;
    padding-bottom: 20px;
`;

const PaddingCard = styled.div`
padding: 10px 20px;
min-height: 302px;

`;
const DescriptionCard = styled.div`
    color: rgb(130, 130, 130);
    font-size: 17px;
    text-align: inherit;
    padding-bottom: 20px;
`;
const TextIconRooms = styled.div`
    background-color:black;
`;
function InfoCard (props){
    return(
        <PaddingCard>
            <TitleCard> {props.name}</TitleCard>
            <DescriptionCard > {props.description}</DescriptionCard>
            <TextIconLocation city={props.city} country={props.country}/>
            <TextIconRooms rooms={props.rooms}  price={props.price}/>
        </PaddingCard>
    );
}