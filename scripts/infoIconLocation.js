
const IconLabel = styled.div`
display: flex;
background-color: #e1e1e1;
padding: 0px;
width: fit-content;
align-items: center;
padding-right: 10px;
margin-bottom: 11px;
border-radius: 3px;
`;
const ImgIcon = styled.img`
width: 20px;
padding: 4px;
height: 18px;
background-color: #0c99f0;
font-size: 14px;
`;
const LabelIcon = styled.div`
color: #2d2c2c;
padding-left: 9px;
`;

function TextIconLocation (props){
    return(
        <IconLabel>
          <ImgIcon src="./images/location.svg"  alt=""/>
            <LabelIcon> {props.city} , {props.country} </LabelIcon>
        </IconLabel>
        

    );
}