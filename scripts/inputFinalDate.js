

const Input = styled.input`
width: 200px;
height: 24px;
margin: 5px 30px;
text-align: center;
line-height: 57px;
font-size: 20px;
`;

class InputFinalDate extends React.Component{
constructor(props) {
    super(props);
    this.changeCalendarFinal = this.changeCalendarFinal.bind(this)
}
changeCalendarFinal(event) {
    this.props.onChangeFinalDate(event.target.value.split("-").join("/"))
}
render() {
   return(
    <Input 
        type="date"  
        value={formatDate(this.props.finalDate, false ,true)} 
        min={formatDate(this.props.initialDate, false ,true)} 
        onChange={this.changeCalendarFinal} 
    />

    );
};
}
