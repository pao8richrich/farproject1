class Application extends React.Component{
 
  constructor(){
    super()
    this.state ={
      selectFilter:{
        initialDate : today, 
        finalDate: today,
        country:'Todos los países' , 
        price:'Cualquier Precio',
        rooms:'Cualquier Tamaño'
      }
    }
  }

  updateInitialDate(value) {

      this.setState(prevState => 
        
        {
        const newState = {
          selectFilter :{
            ...prevState.selectFilter,
            initialDate: new Date(value.split("-").reverse().join("/")),
          }
        }
        if (newState.selectFilter.initialDate > prevState.selectFilter.finalDate){
          newState.selectFilter.finalDate = newState.selectFilter.initialDate
        }
       
        if (newState.selectFilter.initialDate.setHours(0,0,0,0) == new Date().setHours(0,0,0,0)) {
          newState.selectFilter.initialDate = new Date()
        }
        return newState
      });
   }
 
   updateFinalDate(value) {
      this.setState(prevState => ({
        selectFilter :{
          ...prevState.selectFilter,
          finalDate:new Date(value.split("-").reverse().join("/")),
        }
      }));
   }

   onChangeSelectCountry(e){
    var value = e.target.value
    this.setState(prevState => ({
      selectFilter:{
        ...prevState.selectFilter,
        country:value
      }
    }))
  }
  onChangeSelectPrice(e){
    var value = e.target.value
    this.setState(prevState => ({
      selectFilter: { 
        ...prevState.selectFilter,
        price: value
      }
    }));
  }
  onChangeSelectSize(e){
    var value = e.target.value
    this.setState(prevState => ({
      selectFilter: { 
        ...prevState.selectFilter,
        rooms: value
      }
    }));
  }
  
  render() {
    const selectFilter = this.state.selectFilter;
  
    return(
      <div>
        <Header name="Rich Trip" initialDate={selectFilter.initialDate}   finalDate={selectFilter.finalDate}  />
        <Nav 
        onChangeInitialDate={this.updateInitialDate.bind(this)} 
        onChangeFinalDate={this.updateFinalDate.bind(this)} 
        initialDate={selectFilter.initialDate} 
        finalDate={selectFilter.finalDate}
        selectCountry={selectFilter.country}
        onChangeSelectCountry={this.onChangeSelectCountry.bind(this)}
        selectPrice={selectFilter.price}
        onChangeSelectPrice={this.onChangeSelectPrice.bind(this)}
        selectSize={selectFilter.rooms}
        onChangeSelectSize={this.onChangeSelectSize.bind(this)}
        />
        <Main
        selectInitial={selectFilter.initialDate}
        selectFinal={selectFilter.finalDate}
        selectCountry={selectFilter.country}
        selectPrice={selectFilter.price}
        selectSize={selectFilter.rooms}
        />
      </div>
    ); 
  };
}

ReactDOM.render(
  <Application/>,
  document.getElementById('App'),
);