
const IconLabel = styled.div`
display: flex;
background-color: #e1e1e1;
padding: 0px;
width: fit-content;
align-items: center;
padding-right: 10px;
margin-bottom: 11px;
border-radius: 3px;
`;
const ImgIcon = styled.img`
width: 20px;
padding: 4px;
height: 18px;
background-color: #0c99f0;
font-size: 14px;
`;
const LabelIcon = styled.div`
color: #2d2c2c;
padding-left: 9px;
`;
const PriceBg = styled.div`
background-color: #f3ad07;
display: flex;
border-radius: 2px;
width: 82px;
padding: 2px;
height: 26px;
align-content: center;
justify-content: center;
align-items: center;
margin-left: 20px;
`;

function TextIconRooms (props){
    const priceArray = []
    for (let index = 0; index < 4; index++) {
        if (index < props.price){
            priceArray.push(true)
        }else {
            priceArray.push(false)
        }
        
    }
    return(

        <div className="inline">
            <IconLabel>
                <ImgIcon src="./images/bed.svg"  alt=""/>
                <LabelIcon> {props.rooms} Habitaciones</LabelIcon>   
            </IconLabel>
            <PriceBg> 
                {priceArray.map(
                    (element,index) => (
                <div key={index} className={element ? "priceActive" : "price"}>$</div>
                ))}
            </PriceBg>
        </div>

    );
}