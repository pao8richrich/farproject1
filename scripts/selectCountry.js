const ImgSelect = styled.img`
    height: initial;
    width: 20px;
`;
const SelectContainer = styled.div`
    width: 100px;
    height: 40px;
    display: contents;
    font-size: 14px;
    width: 100%;
`;
const Select = styled.select`
    width: 100%;
    height: 40px;
    position: relative;

    // padding: 0 40px;
    // font-size: 16px;
    // appearance: none;
    // -moz-appearance: none;
    // -o-appearance: none;
    // border-radius: 2px;
    // box-shadow: none;
     &:before{
        background-image: url(../images/arrow-down.svg);
        position: absolute;
        right: 15px;
        top: 12px;
          
     }
`;


class SelectCountry extends React.Component{
     constructor(props) {
        super(props);
        this.hotelesFiltrados = this.hotelesFiltrados.bind(this)
    }
    hotelesFiltrados(){
        const hotelesFiltrados = hotelsData.filter(hotelObjet => hotelObjet.country )

    }
    render() {
        const opcionHotel = hotelsData.map(element => (element.country))
        const opcionHotelFinal = [...new Set(opcionHotel)].sort()
        opcionHotelFinal.unshift('Todos los países')
        
       return(
        <SelectContainer>
            <ImgSelect src="images/SelectPaisWhite.svg" alt="Países" />
           
               <Select 
               value={this.props.selectCountry}
               onChange={this.props.onChangeSelectCountry}>
                    {opcionHotelFinal.map(
                        (opcionHotel,index) => {
                        
                        return <option  key={index} value={opcionHotel} >{opcionHotel}</option>
                        }
                    )}
                </Select>
        </SelectContainer>
    
        );
    };
    }
