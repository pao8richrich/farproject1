function formatDate(date,addDay,shortDate) {
    
    if(addDay){
      let date = new Date(date)
      date.setDate(date.getDate()+1)
    }
    const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric',};
    let formattedDate = new Date(date).toLocaleDateString("es-CO", options);
    
    if (shortDate) {
      formattedDate = new Date(date).toLocaleDateString('es-CO', {  
        year:'numeric',  month: '2-digit',  day: '2-digit',  }).split("/").reverse().join("-");
    } 
    return formattedDate
  }