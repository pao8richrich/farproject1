const ImgSelect = styled.img`
    height: initial;
    width: 20px;
`;
const SelectContainer = styled.div`
    width: 100px;
    height: 40px;
    display: contents;
    font-size: 14px;
    width: 100%;
`;
const Select = styled.select`
    width: 100%;
    height: 40px;
    position: relative;

    // padding: 0 40px;
    // font-size: 16px;
    // appearance: none;
    // -moz-appearance: none;
    // -o-appearance: none;
    // border-radius: 2px;
    // box-shadow: none;
     &:before{
        background-image: url(../images/arrow-down.svg);
        position: absolute;
        right: 15px;
        top: 12px;
          
     }
`;


class SelectPrice extends React.Component{
    constructor(props) {
        super(props);
        this.PriceFilter = this.PriceFilter.bind(this)
    }
    PriceFilter(){
        const PriceFilter = hotelsData.filter(hotelObjet => hotelObjet.price )
    }
    render() {
        const opcionPrice = hotelsData.map(element => (element.price))
        const opcionPriceSort = [...new Set(opcionPrice)].sort()
        const opcionPriceFinal = opcionPriceSort.map(
            function(num) {
                return "$".repeat(num);
        })

        opcionPriceFinal.unshift('Cualquier Precio')
       
       return(
        <SelectContainer>
            <ImgSelect src="images/SelectPrecioWhite.svg" alt="Precio" />
               <Select 
               value={this.props.selectPrice}
               onChange={this.props.onChangeSelectPrice}>
                    {opcionPriceFinal.map(
                        (opcionPrice,index) => {
                        return <option key={index} value={opcionPrice} >{opcionPrice}</option>
                        }
                    )}
                </Select>
        </SelectContainer>
    
        );
    };
}