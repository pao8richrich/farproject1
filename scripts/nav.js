const TheNav = styled.nav`
     background-color: #ffb403;
    padding: 0px 10px;
    display: flex;
    flex-wrap: nowrap;
    height:80px;
`;

class Nav extends React.Component{
    constructor(props) {
        super(props);
        this.changeCalendarInitial = this.changeCalendarInitial.bind(this)
        this.changeCalendarFinal = this.changeCalendarFinal.bind(this)
        
    }
    changeCalendarInitial(value) {
        this.props.onChangeInitialDate(value)
    }  
    changeCalendarFinal(value) {
        this.props.onChangeFinalDate(value)
    }  
    render() {
       return(
           
        <TheNav >
            <InputInitialDate 
            onChangeInitialDate={this.changeCalendarInitial} 
            initialDate={this.props.initialDate} 
            finalDate={this.props.finalDate}
            />
            <InputFinalDate 
            onChangeFinalDate={this.changeCalendarFinal}
            initialDate={this.props.initialDate} 
            finalDate={this.props.finalDate}
           
            />
            <SelectCountry 
            selectCountry={this.props.selectedCountry}
            onChangeSelectCountry={this.props.onChangeSelectCountry}
             />
            <SelectPrice
            selectPrice={this.props.price}
            onChangeSelectPrice={this.props.onChangeSelectPrice}
            />
            <SelectSize 
             selectSize={this.props.rooms}
             onChangeSelectSize={this.props.onChangeSelectSize}
            />
        </TheNav>
    );
    };
}
