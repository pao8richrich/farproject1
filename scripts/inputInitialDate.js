

const Input = styled.input`
    width: 200px;
    height: 24px;
    margin: 5px 30px;
    text-align: center;
    line-height: 57px;
    font-size: 20px;
`;

class InputInitialDate  extends React.Component{
    constructor(props) {
        super(props);
       
        this.changeCalendarInitial = this.changeCalendarInitial.bind(this)
    }
    changeCalendarInitial(event) {
        this.props.onChangeInitialDate(event.target.value.split("-").join("/"))
    }  

    
    render() {
      
      
       return(
        <Input 
            type="date" 
            value={formatDate(this.props.initialDate, false ,true)} 
            min={formatDate(today, false ,true)} 
            onChange={this.changeCalendarInitial } 
        />
  
        );
    };
  }
