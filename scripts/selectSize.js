const ImgSelect = styled.img`
    height: initial;
    width: 20px;
`;
const SelectContainer = styled.div`
    width: 100px;
    height: 40px;
    display: contents;
    font-size: 14px;
    width: 100%;
`;
const Select = styled.select`
    width: 100%;
    height: 40px;
    position: relative;
    // padding: 0 40px;
    // font-size: 16px;
    // appearance: none;
    // -moz-appearance: none;
    // -o-appearance: none;
    // border-radius: 2px;
    // box-shadow: none;
    
    &:before{
        background-image: url(../images/arrow-down.svg);
        position: absolute;
        right: 15px;
        top: 12px;
    }
`;


class SelectSize extends React.Component{
    constructor(props) {
        super(props);
        this.sizeFilter = this.sizeFilter.bind(this)
    }
    sizeFilter(){
        const sizeFilter = hotelsData.filter(hotelObjet => hotelObjet.rooms )
    }
    render() {
        const opcionSizeFinal = ["Cualquier Tamaño", "Hotel pequeño", "Hotel mediano", "Hotel grande"]
        
       return(
        <SelectContainer>
            <ImgSelect src="images/SelectBedWhite.svg" alt="rooms" />
               <Select 
               value={this.props.selectSize}
               onChange={this.props.onChangeSelectSize}>
                    {opcionSizeFinal.map(
                        (opcionSize,index) => {
                        return <option key={index} value={opcionSize}>{opcionSize}</option>
                        }
                    )}
                    
                </Select>
        </SelectContainer>
    
        );
    };
}