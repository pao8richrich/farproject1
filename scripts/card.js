const TheCard = styled.div`
    border: solid rgb(230, 230, 230) 1px;
    border-radius: 2px;
    background-color: white;
    -webkit-box-shadow: -1px 2px 7px 0px rgba(0,0,0,0.25);
    -moz-box-shadow: -1px 2px 7px 0px rgba(0,0,0,0.25);
    box-shadow: -1px 2px 7px 0px rgba(0,0,0,0.25);
    width: 30%;
    margin: 16px;
    border-radius: 15px;
    overflow: hidden;
`;
const ImgHotel = styled.img`
    height: auto;
    width: 100%;
`;
const ButtonCard = styled.button`
    text-rendering: auto;
    color: white;
    letter-spacing: normal;
    text-transform: none;
    text-indent: 0px;
    text-shadow: none;
    display: inline-block;
    text-align: center;
    align-items: flex-start;
    cursor: default;
    background-color: #1a60a8;
    box-sizing: border-box;
    margin: 0em;
    font: 400 13.3333px Arial;
    padding: 1px 6px;
    border: solid 1px #1a60a8;
    width: 100%;
    height: 50px;
    font-size: 18px;
    bottom:0;
    
    
    &:hover{
    background-color: #1a60a8;
    color: white;
    }
    &:focus{
        outline: none;
    }
`;

function Card (props){
    return(
        <TheCard>
          <ImgHotel src={props.hotel.photo} alt="" />
          <InfoCard 
            name={props.hotel.name} 
            rooms={props.hotel.rooms}
            description={props.hotel.description}
            city={props.hotel.city}
            country={props.hotel.country}
            price={props.hotel.price}
          />
          <ButtonCard>Reservar</ButtonCard>
        </TheCard>
    );
}